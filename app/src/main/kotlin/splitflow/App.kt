package splitflow

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactor.asFlux
import reactor.core.publisher.Flux

suspend fun generateFlow(): Flow<Char> = flow {
    // In our real application, the flow is composed of single metadata
    // instance, which is followed by blocks of file data.  In this
    // example, the flow is composed, as follows:
    //  (last) BBBBBBBBBBM (first)
    // Emit metadata to the flow...
    emit('M')
    delay(500)
    // Emit block of bytes from a file to the flow...
    for (i in 1..10) {
        emit('B')
        delay(100)
    }
}

suspend fun main(args: Array<String>) {
    var metadata: Char = ' '
    var counter: Long = 0

    println("Configuring the flow that will be used when it is collected.")
    val flowOfBlocks = generateFlow()

    println("Uploading the flow of blocks of data from a file, when the flow is collected.")
    flowOfBlocks
        .map { ch ->
            when (ch) {
                'M' -> {
                    metadata = ch
                    println("*** Received the metadata and saved it.")
                }
                // Since the metadata has been processed,
                // the rest of the items are blocks of bytes from a file.
                else -> {
                    println("(${++counter}) Emitting '$ch' from the flow to another method to push to the Cloud.")
                    // This version works, but does not meet our requirements.
                    // Although we are in a flow and can send a block to another method
                    // to push to the Cloud, the following method simply calls the other one
                    // with a block of data.  In our real system, it expects a flow of data.
                    flowOfBlocks.uploadFlowToCloud(metadata)
                }
            }
        }
}

// Forward the Flow to another method that will upload it to the Cloud.
suspend fun Flow<Char>.uploadFlowToCloud(metadata: Char) {
    delay(250)
    azureBlobServicesSdkUploadFile(metadata, this.asFlux())
}

suspend fun azureBlobServicesSdkUploadFile(metadata: Char, content: Flux<Char>) {
    delay(500)
    content.map { blockOfData ->
        println("Sent Block To Cloud: '$blockOfData' with metadata '$metadata")
    }.subscribe()
}
